package it.siw.ProgettoSiw.repository;

import org.springframework.data.repository.CrudRepository;

import it.siw.ProgettoSiw.model.Centro;

public interface CentroRepository extends CrudRepository<Centro, Long> {

}
