package it.siw.ProgettoSiw.repository;

import org.springframework.data.repository.CrudRepository;

import it.siw.ProgettoSiw.security.Utente;

public interface UserRepository extends CrudRepository<Utente, Long>{

}
