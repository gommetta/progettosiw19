package it.siw.ProgettoSiw.controller;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

	
	@RequestMapping("/")
	public String redirectToLogin() {
		return "redirect:/login";
	}
	
	@RequestMapping("/login")
	public String login(Authentication auth) {
		boolean isAdminCentro=false;
		boolean isAdminGlobale = false;
		if(auth!=null) {
			Collection<? extends GrantedAuthority> authorities=auth.getAuthorities();
			for (GrantedAuthority a : authorities) {
				if(a.getAuthority().equals("ROLE_ADMIN")) {
					isAdminGlobale=true;
				}
				if(a.getAuthority().equals("ROLE_ADMIN-CENTRO")) {
					isAdminCentro=true;
				}
				
			}
		}
		if(isAdminCentro) {
			System.out.println("\n\nloggato come amministratore centro\n\n");
			return "redirect:/amministrazione-centro";
		}
		else if(isAdminGlobale) {
			System.out.println("\n\nloggato come amministratore globale\n\n");
			return "redirect:/amministrazione-globale";
		}
		return "login";
	}
	
	@RequestMapping("/login-error")
	public String loginErr(Model m) {
		m.addAttribute("error", true);
		return "login";
	}

}
