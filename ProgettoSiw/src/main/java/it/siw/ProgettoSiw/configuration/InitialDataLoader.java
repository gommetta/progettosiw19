package it.siw.ProgettoSiw.configuration;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import it.siw.ProgettoSiw.security.Privilege;
import it.siw.ProgettoSiw.security.Role;
import it.siw.ProgettoSiw.security.Utente;
import it.siw.ProgettoSiw.service.PrivilegeService;
import it.siw.ProgettoSiw.service.RoleService;
import it.siw.ProgettoSiw.service.UserService;

@Component
public class InitialDataLoader implements
  ApplicationListener<ContextRefreshedEvent> {
 
    boolean alreadySetup = false;
 
    @Autowired
    private UserService userService;
  
    @Autowired
    private RoleService roleService;
  
    @Autowired
    private PrivilegeService privilegeService;
  
//    @Autowired
//    private BCryptPasswordEncoder passwordEncoder;
  
    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
  
        if (alreadySetup)
            return;
        Privilege readPrivilege
          = createPrivilegeIfNotFound("READ_PRIVILEGE");
        Privilege writePrivilege
          = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
  
        List<Privilege> adminPrivileges = Arrays.asList(
          readPrivilege, writePrivilege);        
        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_ADMIN_CENTRO", adminPrivileges);
 
        Role adminRole = roleService.findByName("ROLE_ADMIN");
        Utente user = new Utente();
        user.setUsername("test");
        user.setPassword("test"/*passwordEncoder.encode("test")*/);
        user.setRoles(Arrays.asList(adminRole));
        user.setEnabled(true);
        userService.save(user);
 
        alreadySetup = true;
    }
 
    @Transactional
    private Privilege createPrivilegeIfNotFound(String name) {
  
        Privilege privilege = privilegeService.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilegeService.save(privilege);
        }
        return privilege;
    }
 
    @Transactional
    private Role createRoleIfNotFound(
      String name, Collection<Privilege> privileges) {
  
        Role role = roleService.findByName(name);
        if (role == null) {
            role = new Role(name);
            role.setPrivileges(privileges);
            roleService.save(role);
        }
        return role;
    }
}
