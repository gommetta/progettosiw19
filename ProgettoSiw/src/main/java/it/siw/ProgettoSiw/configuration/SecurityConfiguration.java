package it.siw.ProgettoSiw.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private DataSource datasource;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(datasource)
		.usersByUsernameQuery("SELECT username,password ,1 FROM usern where enabled=true and username=?")
		.authoritiesByUsernameQuery("SELECT username,role FROM usern U, role R, usern_roles UR  where UR.users_id = U.id and UR.roles_id = R.id and username=?");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
		.authorizeRequests()
			.antMatchers("/centro","/centro/**").hasRole("ADMIN_CENTRO")
			.antMatchers("/admin","/admin/**").hasRole("ADMIN")
			.and()
		.formLogin()
			.loginPage("/login")
			.permitAll()
			.failureUrl("/login-error")
			.and()
		.logout()
			.logoutUrl("/logout")
			.logoutSuccessUrl("/login")
			.permitAll();
	}

}
