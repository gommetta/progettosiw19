package it.siw.ProgettoSiw.model;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Allievo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column
	private String nome;
	private String email;
	private String tel;
	@Temporal(TemporalType.DATE)
	private Calendar dataNascita;
	
	@ManyToMany
	private List<Attivita> attivitaSvolte;

	
	public boolean equals(Object o) {
		Allievo a =(Allievo) o ;
		return a.getNome().equals(this.getNome()) && a.getEmail().equals(this.getEmail());
	}
	
	public int hashCode() {
		return this.getNome().hashCode() + this.getEmail().hashCode();
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Calendar getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Calendar dataNascita) {
		this.dataNascita = dataNascita;
	}

	public List<Attivita> getAttivitaSvolte() {
		return attivitaSvolte;
	}

	public void setAttivitaSvolte(List<Attivita> attivitaSvolte) {
		this.attivitaSvolte = attivitaSvolte;
	}

	public Long getId() {
		return id;
	}
	
}
