package it.siw.ProgettoSiw.model;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Attivita {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long Id;
	
	@Column
	private String nome;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataEOra;
	
	@ManyToMany(mappedBy="attivitaSvolte")
	private List<Allievo> allievi;

	
	public boolean equals(Object o) {
		Attivita a=(Attivita)o;
		return a.getNome().equals(this.getNome()) && a.getDataEOra().equals(this.getDataEOra());
	}
	
	public int hashCode() {
		return this.getNome().hashCode() + this.getDataEOra().hashCode();
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDataEOra() {
		return dataEOra;
	}

	public void setDataEOra(Calendar dataEOra) {
		this.dataEOra = dataEOra;
	}

	public List<Allievo> getAllievi() {
		return allievi;
	}

	public void setAllievi(List<Allievo> allievi) {
		this.allievi = allievi;
	}

	public Long getId() {
		return Id;
	}
}
