package it.siw.ProgettoSiw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.siw.ProgettoSiw.model.Attivita;
import it.siw.ProgettoSiw.repository.AttivitaRepository;

@Service
public class AttivitaService {

	
	@Autowired
	private AttivitaRepository attivitaRepository;
	
	public Attivita findById(Long id) {
		return attivitaRepository.findOne(id);
	}
}
