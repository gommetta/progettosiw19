package it.siw.ProgettoSiw.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.siw.ProgettoSiw.model.Allievo;
import it.siw.ProgettoSiw.repository.AllievoRepository;

@Service
public class AllievoService {

	
	@Autowired
	private AllievoRepository allievoRepository;
	
	public Allievo findById(Long id) {
		return allievoRepository.findOne(id);
	}
	
	public List<Allievo> findAll(){
		List<Allievo> l = new ArrayList<Allievo>();
		l.addAll(allievoRepository.findAll());
		return l;
	}
}
