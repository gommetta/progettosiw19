package it.siw.ProgettoSiw.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.siw.ProgettoSiw.model.Attivita;
import it.siw.ProgettoSiw.model.Centro;
import it.siw.ProgettoSiw.repository.CentroRepository;

@Service
public class CentroService {

	
	@Autowired
	private CentroRepository centroRepository;
	
	public Centro findById(Long id) {
		return centroRepository.findOne(id);
	}
	
	public List<Centro> findALL(){
		Arra	yList l = new ArrayList<>();
		return centroRepository.findAll();
	}
	
}
