package it.siw.ProgettoSiw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.siw.ProgettoSiw.repository.RoleRepository;
import it.siw.ProgettoSiw.security.Role;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;
	
	@Transactional
	public void save(Role r) {
		this.roleRepository.save(r);
	}

	public Role findByName(String name) {
		for (Role role : this.roleRepository.findAll()) {
			if(role.getName().equals(name)) {
				return role;
			}
		}
		return null;
	}
}